﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace player
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    /// The class representing the playerRepository element repository.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")]
    [RepositoryFolder("66f61d9d-296f-4683-8639-34b1eee29517")]
    public partial class playerRepository : RepoGenBaseFolder
    {
        static playerRepository instance = new playerRepository();
        playerRepositoryFolders.CFRAME2AppFolder _cframe2;
        playerRepositoryFolders.OpenAppFolder _open;

        /// <summary>
        /// Gets the singleton class instance representing the playerRepository element repository.
        /// </summary>
        [RepositoryFolder("66f61d9d-296f-4683-8639-34b1eee29517")]
        public static playerRepository Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Repository class constructor.
        /// </summary>
        public playerRepository() 
            : base("playerRepository", "/", null, 0, false, "66f61d9d-296f-4683-8639-34b1eee29517", ".\\RepositoryImages\\playerRepository66f61d9d.rximgres")
        {
            _cframe2 = new playerRepositoryFolders.CFRAME2AppFolder(this);
            _open = new playerRepositoryFolders.OpenAppFolder(this);
        }

#region Variables

#endregion

        /// <summary>
        /// The Self item info.
        /// </summary>
        [RepositoryItemInfo("66f61d9d-296f-4683-8639-34b1eee29517")]
        public virtual RepoItemInfo SelfInfo
        {
            get
            {
                return _selfInfo;
            }
        }

        /// <summary>
        /// The CFRAME2 folder.
        /// </summary>
        [RepositoryFolder("a6ccb9b3-65c2-4ae9-a0ce-0218c71c7249")]
        public virtual playerRepositoryFolders.CFRAME2AppFolder CFRAME2
        {
            get { return _cframe2; }
        }

        /// <summary>
        /// The Open folder.
        /// </summary>
        [RepositoryFolder("a2a85bee-fe65-4c84-aab3-40f913f94e87")]
        public virtual playerRepositoryFolders.OpenAppFolder Open
        {
            get { return _open; }
        }
    }

    /// <summary>
    /// Inner folder classes.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")]
    public partial class playerRepositoryFolders
    {
        /// <summary>
        /// The CFRAME2AppFolder folder.
        /// </summary>
        [RepositoryFolder("a6ccb9b3-65c2-4ae9-a0ce-0218c71c7249")]
        public partial class CFRAME2AppFolder : RepoGenBaseFolder
        {
            playerRepositoryFolders.ListFolder _list;
            RepoItemInfo _clearconfigInfo;
            RepoItemInfo _clientInfo;
            RepoItemInfo _openInfo;
            RepoItemInfo _playInfo;
            RepoItemInfo _spinbuttonInfo;
            RepoItemInfo _moduleclassInfo;
            RepoItemInfo _outlineitemInfo;
            RepoItemInfo _stopInfo;
            RepoItemInfo _reloadInfo;

            /// <summary>
            /// Creates a new CFRAME2  folder.
            /// </summary>
            public CFRAME2AppFolder(RepoGenBaseFolder parentFolder) :
                    base("CFRAME2", "/form[@processname='CFrame2' and @class='Qt5QWindowIcon' and @instance='2']", parentFolder, 30000, null, true, "a6ccb9b3-65c2-4ae9-a0ce-0218c71c7249", "")
            {
                _list = new playerRepositoryFolders.ListFolder(this);
                _clearconfigInfo = new RepoItemInfo(this, "ClearConfig", "?/?/button[@accessiblename='Clear Config']", 30000, null, "29ef79ee-16d3-4027-8fe0-05ab4c542836");
                _clientInfo = new RepoItemInfo(this, "Client", "container[2]/container[@accessiblerole='Client']", 30000, null, "33fcc659-c473-4841-b3cc-447e67ea9463");
                _openInfo = new RepoItemInfo(this, "Open", "?/?/button[@accessiblename='Open']", 30000, null, "0666540b-813e-4373-814b-e5cd982838a8");
                _playInfo = new RepoItemInfo(this, "Play", "?/?/button[@accessiblename='Play']", 30000, null, "e08f32ee-ed4a-4682-975d-9182b3b6e276");
                _spinbuttonInfo = new RepoItemInfo(this, "SpinButton", "container[7]/container[@accessiblerole='SpinButton']", 30000, null, "f216f966-8ac7-457d-a962-8cea5c8fa7b7");
                _moduleclassInfo = new RepoItemInfo(this, "ModuleClass", "?/?/treeitem[@accessiblename='Module Class']", 30000, null, "4aee9f34-849f-4a86-84f9-f62881296595");
                _outlineitemInfo = new RepoItemInfo(this, "OutlineItem", "tree[2]/treeitem[12]", 30000, null, "1f7adcec-69f2-4a5d-952f-3ab525f97162");
                _stopInfo = new RepoItemInfo(this, "Stop", "?/?/button[@accessiblename='Stop']", 30000, null, "dd3a1dc6-0005-4928-a306-656992dfffb3");
                _reloadInfo = new RepoItemInfo(this, "Reload", "?/?/button[@accessiblename='Reload']", 30000, null, "726d8f71-02d6-40cf-8185-ae5e20238b31");
            }

            /// <summary>
            /// The Self item.
            /// </summary>
            [RepositoryItem("a6ccb9b3-65c2-4ae9-a0ce-0218c71c7249")]
            public virtual Ranorex.Form Self
            {
                get
                {
                    return _selfInfo.CreateAdapter<Ranorex.Form>(true);
                }
            }

            /// <summary>
            /// The Self item info.
            /// </summary>
            [RepositoryItemInfo("a6ccb9b3-65c2-4ae9-a0ce-0218c71c7249")]
            public virtual RepoItemInfo SelfInfo
            {
                get
                {
                    return _selfInfo;
                }
            }

            /// <summary>
            /// The ClearConfig item.
            /// </summary>
            [RepositoryItem("29ef79ee-16d3-4027-8fe0-05ab4c542836")]
            public virtual Ranorex.Button ClearConfig
            {
                get
                {
                    return _clearconfigInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The ClearConfig item info.
            /// </summary>
            [RepositoryItemInfo("29ef79ee-16d3-4027-8fe0-05ab4c542836")]
            public virtual RepoItemInfo ClearConfigInfo
            {
                get
                {
                    return _clearconfigInfo;
                }
            }

            /// <summary>
            /// The Client item.
            /// </summary>
            [RepositoryItem("33fcc659-c473-4841-b3cc-447e67ea9463")]
            public virtual Ranorex.Container Client
            {
                get
                {
                    return _clientInfo.CreateAdapter<Ranorex.Container>(true);
                }
            }

            /// <summary>
            /// The Client item info.
            /// </summary>
            [RepositoryItemInfo("33fcc659-c473-4841-b3cc-447e67ea9463")]
            public virtual RepoItemInfo ClientInfo
            {
                get
                {
                    return _clientInfo;
                }
            }

            /// <summary>
            /// The Open item.
            /// </summary>
            [RepositoryItem("0666540b-813e-4373-814b-e5cd982838a8")]
            public virtual Ranorex.Button Open
            {
                get
                {
                    return _openInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The Open item info.
            /// </summary>
            [RepositoryItemInfo("0666540b-813e-4373-814b-e5cd982838a8")]
            public virtual RepoItemInfo OpenInfo
            {
                get
                {
                    return _openInfo;
                }
            }

            /// <summary>
            /// The Play item.
            /// </summary>
            [RepositoryItem("e08f32ee-ed4a-4682-975d-9182b3b6e276")]
            public virtual Ranorex.Button Play
            {
                get
                {
                    return _playInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The Play item info.
            /// </summary>
            [RepositoryItemInfo("e08f32ee-ed4a-4682-975d-9182b3b6e276")]
            public virtual RepoItemInfo PlayInfo
            {
                get
                {
                    return _playInfo;
                }
            }

            /// <summary>
            /// The SpinButton item.
            /// </summary>
            [RepositoryItem("f216f966-8ac7-457d-a962-8cea5c8fa7b7")]
            public virtual Ranorex.Container SpinButton
            {
                get
                {
                    return _spinbuttonInfo.CreateAdapter<Ranorex.Container>(true);
                }
            }

            /// <summary>
            /// The SpinButton item info.
            /// </summary>
            [RepositoryItemInfo("f216f966-8ac7-457d-a962-8cea5c8fa7b7")]
            public virtual RepoItemInfo SpinButtonInfo
            {
                get
                {
                    return _spinbuttonInfo;
                }
            }

            /// <summary>
            /// The ModuleClass item.
            /// </summary>
            [RepositoryItem("4aee9f34-849f-4a86-84f9-f62881296595")]
            public virtual Ranorex.TreeItem ModuleClass
            {
                get
                {
                    return _moduleclassInfo.CreateAdapter<Ranorex.TreeItem>(true);
                }
            }

            /// <summary>
            /// The ModuleClass item info.
            /// </summary>
            [RepositoryItemInfo("4aee9f34-849f-4a86-84f9-f62881296595")]
            public virtual RepoItemInfo ModuleClassInfo
            {
                get
                {
                    return _moduleclassInfo;
                }
            }

            /// <summary>
            /// The OutlineItem item.
            /// </summary>
            [RepositoryItem("1f7adcec-69f2-4a5d-952f-3ab525f97162")]
            public virtual Ranorex.TreeItem OutlineItem
            {
                get
                {
                    return _outlineitemInfo.CreateAdapter<Ranorex.TreeItem>(true);
                }
            }

            /// <summary>
            /// The OutlineItem item info.
            /// </summary>
            [RepositoryItemInfo("1f7adcec-69f2-4a5d-952f-3ab525f97162")]
            public virtual RepoItemInfo OutlineItemInfo
            {
                get
                {
                    return _outlineitemInfo;
                }
            }

            /// <summary>
            /// The Stop item.
            /// </summary>
            [RepositoryItem("dd3a1dc6-0005-4928-a306-656992dfffb3")]
            public virtual Ranorex.Button Stop
            {
                get
                {
                    return _stopInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The Stop item info.
            /// </summary>
            [RepositoryItemInfo("dd3a1dc6-0005-4928-a306-656992dfffb3")]
            public virtual RepoItemInfo StopInfo
            {
                get
                {
                    return _stopInfo;
                }
            }

            /// <summary>
            /// The Reload item.
            /// </summary>
            [RepositoryItem("726d8f71-02d6-40cf-8185-ae5e20238b31")]
            public virtual Ranorex.Button Reload
            {
                get
                {
                    return _reloadInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The Reload item info.
            /// </summary>
            [RepositoryItemInfo("726d8f71-02d6-40cf-8185-ae5e20238b31")]
            public virtual RepoItemInfo ReloadInfo
            {
                get
                {
                    return _reloadInfo;
                }
            }

            /// <summary>
            /// The List folder.
            /// </summary>
            [RepositoryFolder("9a3233bd-8dd2-45f3-96b9-bde09928e19e")]
            public virtual playerRepositoryFolders.ListFolder List
            {
                get { return _list; }
            }
        }

        /// <summary>
        /// The ListFolder folder.
        /// </summary>
        [RepositoryFolder("9a3233bd-8dd2-45f3-96b9-bde09928e19e")]
        public partial class ListFolder : RepoGenBaseFolder
        {
            RepoItemInfo _jpegdecoderInfo;
            RepoItemInfo _jpegencoderInfo;
            RepoItemInfo _pclviewInfo;
            RepoItemInfo _playerInfo;
            RepoItemInfo _videowidgetInfo;

            /// <summary>
            /// Creates a new List  folder.
            /// </summary>
            public ListFolder(RepoGenBaseFolder parentFolder) :
                    base("List", "container[1]//list[@accessiblerole='List']", parentFolder, 30000, null, false, "9a3233bd-8dd2-45f3-96b9-bde09928e19e", "")
            {
                _jpegdecoderInfo = new RepoItemInfo(this, "JpegDecoder", "listitem[@accessiblename='JpegDecoder']", 30000, null, "577965cd-c320-4859-8b3b-7f98ce1ef905");
                _jpegencoderInfo = new RepoItemInfo(this, "JpegEncoder", "listitem[@accessiblename='JpegEncoder']", 30000, null, "1ee92ad5-7e38-49d2-8f70-6d4b477d3b1c");
                _pclviewInfo = new RepoItemInfo(this, "PclView", "listitem[@accessiblename='PclView']", 30000, null, "f6d4bcbd-329e-487f-bfe9-1cd2a326d8fa");
                _playerInfo = new RepoItemInfo(this, "Player", "listitem[@accessiblename='Player']", 30000, null, "3975242e-f8e7-4a5e-ac6d-4513a1514b59");
                _videowidgetInfo = new RepoItemInfo(this, "VideoWidget", "listitem[@accessiblename='VideoWidget']", 30000, null, "1595c2a3-678c-4db9-b916-abe0875857d1");
            }

            /// <summary>
            /// The Self item.
            /// </summary>
            [RepositoryItem("9a3233bd-8dd2-45f3-96b9-bde09928e19e")]
            public virtual Ranorex.List Self
            {
                get
                {
                    return _selfInfo.CreateAdapter<Ranorex.List>(true);
                }
            }

            /// <summary>
            /// The Self item info.
            /// </summary>
            [RepositoryItemInfo("9a3233bd-8dd2-45f3-96b9-bde09928e19e")]
            public virtual RepoItemInfo SelfInfo
            {
                get
                {
                    return _selfInfo;
                }
            }

            /// <summary>
            /// The JpegDecoder item.
            /// </summary>
            [RepositoryItem("577965cd-c320-4859-8b3b-7f98ce1ef905")]
            public virtual Ranorex.ListItem JpegDecoder
            {
                get
                {
                    return _jpegdecoderInfo.CreateAdapter<Ranorex.ListItem>(true);
                }
            }

            /// <summary>
            /// The JpegDecoder item info.
            /// </summary>
            [RepositoryItemInfo("577965cd-c320-4859-8b3b-7f98ce1ef905")]
            public virtual RepoItemInfo JpegDecoderInfo
            {
                get
                {
                    return _jpegdecoderInfo;
                }
            }

            /// <summary>
            /// The JpegEncoder item.
            /// </summary>
            [RepositoryItem("1ee92ad5-7e38-49d2-8f70-6d4b477d3b1c")]
            public virtual Ranorex.ListItem JpegEncoder
            {
                get
                {
                    return _jpegencoderInfo.CreateAdapter<Ranorex.ListItem>(true);
                }
            }

            /// <summary>
            /// The JpegEncoder item info.
            /// </summary>
            [RepositoryItemInfo("1ee92ad5-7e38-49d2-8f70-6d4b477d3b1c")]
            public virtual RepoItemInfo JpegEncoderInfo
            {
                get
                {
                    return _jpegencoderInfo;
                }
            }

            /// <summary>
            /// The PclView item.
            /// </summary>
            [RepositoryItem("f6d4bcbd-329e-487f-bfe9-1cd2a326d8fa")]
            public virtual Ranorex.ListItem PclView
            {
                get
                {
                    return _pclviewInfo.CreateAdapter<Ranorex.ListItem>(true);
                }
            }

            /// <summary>
            /// The PclView item info.
            /// </summary>
            [RepositoryItemInfo("f6d4bcbd-329e-487f-bfe9-1cd2a326d8fa")]
            public virtual RepoItemInfo PclViewInfo
            {
                get
                {
                    return _pclviewInfo;
                }
            }

            /// <summary>
            /// The Player item.
            /// </summary>
            [RepositoryItem("3975242e-f8e7-4a5e-ac6d-4513a1514b59")]
            public virtual Ranorex.ListItem Player
            {
                get
                {
                    return _playerInfo.CreateAdapter<Ranorex.ListItem>(true);
                }
            }

            /// <summary>
            /// The Player item info.
            /// </summary>
            [RepositoryItemInfo("3975242e-f8e7-4a5e-ac6d-4513a1514b59")]
            public virtual RepoItemInfo PlayerInfo
            {
                get
                {
                    return _playerInfo;
                }
            }

            /// <summary>
            /// The VideoWidget item.
            /// </summary>
            [RepositoryItem("1595c2a3-678c-4db9-b916-abe0875857d1")]
            public virtual Ranorex.ListItem VideoWidget
            {
                get
                {
                    return _videowidgetInfo.CreateAdapter<Ranorex.ListItem>(true);
                }
            }

            /// <summary>
            /// The VideoWidget item info.
            /// </summary>
            [RepositoryItemInfo("1595c2a3-678c-4db9-b916-abe0875857d1")]
            public virtual RepoItemInfo VideoWidgetInfo
            {
                get
                {
                    return _videowidgetInfo;
                }
            }
        }

        /// <summary>
        /// The OpenAppFolder folder.
        /// </summary>
        [RepositoryFolder("a2a85bee-fe65-4c84-aab3-40f913f94e87")]
        public partial class OpenAppFolder : RepoGenBaseFolder
        {
            playerRepositoryFolders.ShellViewFolder _shellview;
            RepoItemInfo _newvolumedInfo;
            RepoItemInfo _buttonopenInfo;

            /// <summary>
            /// Creates a new Open  folder.
            /// </summary>
            public OpenAppFolder(RepoGenBaseFolder parentFolder) :
                    base("Open", "/form[@title='Open']", parentFolder, 30000, null, true, "a2a85bee-fe65-4c84-aab3-40f913f94e87", "")
            {
                _shellview = new playerRepositoryFolders.ShellViewFolder(this);
                _newvolumedInfo = new RepoItemInfo(this, "NewVolumeD", "element[@class='DUIViewWndClassName']/container/element[1]/?/?/tree[@controlid='100']//treeitem[@text='New Volume (D:)']", 30000, null, "fcab6a9b-1d27-418d-9be7-ea1184326d07");
                _buttonopenInfo = new RepoItemInfo(this, "ButtonOpen", "button[@text='&Open']", 30000, null, "056a3e73-24f1-47a4-99e6-929ba5dcf956");
            }

            /// <summary>
            /// The Self item.
            /// </summary>
            [RepositoryItem("a2a85bee-fe65-4c84-aab3-40f913f94e87")]
            public virtual Ranorex.Form Self
            {
                get
                {
                    return _selfInfo.CreateAdapter<Ranorex.Form>(true);
                }
            }

            /// <summary>
            /// The Self item info.
            /// </summary>
            [RepositoryItemInfo("a2a85bee-fe65-4c84-aab3-40f913f94e87")]
            public virtual RepoItemInfo SelfInfo
            {
                get
                {
                    return _selfInfo;
                }
            }

            /// <summary>
            /// The NewVolumeD item.
            /// </summary>
            [RepositoryItem("fcab6a9b-1d27-418d-9be7-ea1184326d07")]
            public virtual Ranorex.TreeItem NewVolumeD
            {
                get
                {
                    return _newvolumedInfo.CreateAdapter<Ranorex.TreeItem>(true);
                }
            }

            /// <summary>
            /// The NewVolumeD item info.
            /// </summary>
            [RepositoryItemInfo("fcab6a9b-1d27-418d-9be7-ea1184326d07")]
            public virtual RepoItemInfo NewVolumeDInfo
            {
                get
                {
                    return _newvolumedInfo;
                }
            }

            /// <summary>
            /// The ButtonOpen item.
            /// </summary>
            [RepositoryItem("056a3e73-24f1-47a4-99e6-929ba5dcf956")]
            public virtual Ranorex.Button ButtonOpen
            {
                get
                {
                    return _buttonopenInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The ButtonOpen item info.
            /// </summary>
            [RepositoryItemInfo("056a3e73-24f1-47a4-99e6-929ba5dcf956")]
            public virtual RepoItemInfo ButtonOpenInfo
            {
                get
                {
                    return _buttonopenInfo;
                }
            }

            /// <summary>
            /// The ShellView folder.
            /// </summary>
            [RepositoryFolder("9d3a5a0e-fe80-4d2b-91cf-6a5b20193d16")]
            public virtual playerRepositoryFolders.ShellViewFolder ShellView
            {
                get { return _shellview; }
            }
        }

        /// <summary>
        /// The ShellViewFolder folder.
        /// </summary>
        [RepositoryFolder("9d3a5a0e-fe80-4d2b-91cf-6a5b20193d16")]
        public partial class ShellViewFolder : RepoGenBaseFolder
        {
            RepoItemInfo _systemitemnamedisplay1Info;
            RepoItemInfo _systemitemnamedisplay2Info;
            RepoItemInfo _systemitemnamedisplayInfo;
            RepoItemInfo _systemitemnamedisplay4Info;
            RepoItemInfo _systemitemnamedisplay3Info;

            /// <summary>
            /// Creates a new ShellView  folder.
            /// </summary>
            public ShellViewFolder(RepoGenBaseFolder parentFolder) :
                    base("ShellView", "element[@class='DUIViewWndClassName']//container[@caption='ShellView']", parentFolder, 30000, null, false, "9d3a5a0e-fe80-4d2b-91cf-6a5b20193d16", "")
            {
                _systemitemnamedisplay1Info = new RepoItemInfo(this, "SystemItemNameDisplay1", "?/?/listitem[@automationid='17']/text[@automationid='System.ItemNameDisplay']", 30000, null, "502300a8-0259-4b1c-a6ea-f3b6b0ceac82");
                _systemitemnamedisplay2Info = new RepoItemInfo(this, "SystemItemNameDisplay2", "?/?/listitem[@automationid='2']/text[@automationid='System.ItemNameDisplay']", 30000, null, "7cd4d45b-7ac8-4a17-9ec5-54a5d5ea8bb7");
                _systemitemnamedisplayInfo = new RepoItemInfo(this, "SystemItemNameDisplay", "?/?/listitem[@automationid='4']/text[@automationid='System.ItemNameDisplay']", 30000, null, "ec22f834-0698-46f2-ba33-11cc5bd8b274");
                _systemitemnamedisplay4Info = new RepoItemInfo(this, "SystemItemNameDisplay4", "?/?/listitem[@automationid='5']/text[@automationid='System.ItemNameDisplay']", 30000, null, "d7b1aba4-6dfc-4e9a-8327-bac445eaa2a8");
                _systemitemnamedisplay3Info = new RepoItemInfo(this, "SystemItemNameDisplay3", "?/?/listitem[@automationid='8']/text[@automationid='System.ItemNameDisplay']", 30000, null, "5dc78734-e523-4307-8684-d9a93d238b4f");
            }

            /// <summary>
            /// The Self item.
            /// </summary>
            [RepositoryItem("9d3a5a0e-fe80-4d2b-91cf-6a5b20193d16")]
            public virtual Ranorex.Container Self
            {
                get
                {
                    return _selfInfo.CreateAdapter<Ranorex.Container>(true);
                }
            }

            /// <summary>
            /// The Self item info.
            /// </summary>
            [RepositoryItemInfo("9d3a5a0e-fe80-4d2b-91cf-6a5b20193d16")]
            public virtual RepoItemInfo SelfInfo
            {
                get
                {
                    return _selfInfo;
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay1 item.
            /// </summary>
            [RepositoryItem("502300a8-0259-4b1c-a6ea-f3b6b0ceac82")]
            public virtual Ranorex.Text SystemItemNameDisplay1
            {
                get
                {
                    return _systemitemnamedisplay1Info.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay1 item info.
            /// </summary>
            [RepositoryItemInfo("502300a8-0259-4b1c-a6ea-f3b6b0ceac82")]
            public virtual RepoItemInfo SystemItemNameDisplay1Info
            {
                get
                {
                    return _systemitemnamedisplay1Info;
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay2 item.
            /// </summary>
            [RepositoryItem("7cd4d45b-7ac8-4a17-9ec5-54a5d5ea8bb7")]
            public virtual Ranorex.Text SystemItemNameDisplay2
            {
                get
                {
                    return _systemitemnamedisplay2Info.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay2 item info.
            /// </summary>
            [RepositoryItemInfo("7cd4d45b-7ac8-4a17-9ec5-54a5d5ea8bb7")]
            public virtual RepoItemInfo SystemItemNameDisplay2Info
            {
                get
                {
                    return _systemitemnamedisplay2Info;
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay item.
            /// </summary>
            [RepositoryItem("ec22f834-0698-46f2-ba33-11cc5bd8b274")]
            public virtual Ranorex.Text SystemItemNameDisplay
            {
                get
                {
                    return _systemitemnamedisplayInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay item info.
            /// </summary>
            [RepositoryItemInfo("ec22f834-0698-46f2-ba33-11cc5bd8b274")]
            public virtual RepoItemInfo SystemItemNameDisplayInfo
            {
                get
                {
                    return _systemitemnamedisplayInfo;
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay4 item.
            /// </summary>
            [RepositoryItem("d7b1aba4-6dfc-4e9a-8327-bac445eaa2a8")]
            public virtual Ranorex.Text SystemItemNameDisplay4
            {
                get
                {
                    return _systemitemnamedisplay4Info.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay4 item info.
            /// </summary>
            [RepositoryItemInfo("d7b1aba4-6dfc-4e9a-8327-bac445eaa2a8")]
            public virtual RepoItemInfo SystemItemNameDisplay4Info
            {
                get
                {
                    return _systemitemnamedisplay4Info;
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay3 item.
            /// </summary>
            [RepositoryItem("5dc78734-e523-4307-8684-d9a93d238b4f")]
            public virtual Ranorex.Text SystemItemNameDisplay3
            {
                get
                {
                    return _systemitemnamedisplay3Info.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The SystemItemNameDisplay3 item info.
            /// </summary>
            [RepositoryItemInfo("5dc78734-e523-4307-8684-d9a93d238b4f")]
            public virtual RepoItemInfo SystemItemNameDisplay3Info
            {
                get
                {
                    return _systemitemnamedisplay3Info;
                }
            }
        }

    }
#pragma warning restore 0436
}