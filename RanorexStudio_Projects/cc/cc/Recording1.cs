﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace cc
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Recording1 recording.
    /// </summary>
    [TestModule("3417f1aa-2b21-4bfd-84d4-aa53691e0a62", ModuleType.Recording, 1)]
    public partial class Recording1 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the ccRepository repository.
        /// </summary>
        public static ccRepository repo = ccRepository.Instance;

        static Recording1 instance = new Recording1();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Recording1()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Recording1 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Application", "Run application 'C:\\CMORE\\CFrame2.1\\bin\\release\\CFrame2.exe' with arguments '' in normal mode.", new RecordItemIndex(0));
            Host.Local.RunApplication("C:\\CMORE\\CFrame2.1\\bin\\release\\CFrame2.exe", "", "C:\\CMORE\\CFrame2.1\\bin\\release", false);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'CFRAME2.Client' at 102;301.", repo.CFRAME2.ClientInfo, new RecordItemIndex(1));
            repo.CFRAME2.Client.DoubleClick("102;301");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'CFRAME2.Client' at 112;318.", repo.CFRAME2.ClientInfo, new RecordItemIndex(2));
            repo.CFRAME2.Client.DoubleClick("112;318");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CFRAME2.ClearConfig' at 57;23.", repo.CFRAME2.ClearConfigInfo, new RecordItemIndex(3));
            repo.CFRAME2.ClearConfig.Click("57;23");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CFRAME2.Reload' at 38;22.", repo.CFRAME2.ReloadInfo, new RecordItemIndex(4));
            repo.CFRAME2.Reload.Click("38;22");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CFRAME2.File' at 35;16.", repo.CFRAME2.FileInfo, new RecordItemIndex(5));
            repo.CFRAME2.File.Click("35;16");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CFRAME.LoadConfiguration' at 62;19.", repo.CFRAME.LoadConfigurationInfo, new RecordItemIndex(6));
            repo.CFRAME.LoadConfiguration.Click("62;19");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'LoadConfiguration.Tree100' at 148;94.", repo.LoadConfiguration.Tree100Info, new RecordItemIndex(7));
            repo.LoadConfiguration.Tree100.Click("148;94");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'LoadConfiguration.BaselineImage' at 36;80.", repo.LoadConfiguration.BaselineImageInfo, new RecordItemIndex(8));
            repo.LoadConfiguration.BaselineImage.DoubleClick("36;80");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'CFRAME2.Client' at 106;296.", repo.CFRAME2.ClientInfo, new RecordItemIndex(9));
            repo.CFRAME2.Client.DoubleClick("106;296");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'CFRAME2.Client' at 167;324.", repo.CFRAME2.ClientInfo, new RecordItemIndex(10));
            repo.CFRAME2.Client.DoubleClick("167;324");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
