﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace CFRAME
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    /// The class representing the CFRAMERepository element repository.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")]
    [RepositoryFolder("e9b7fddc-ebe4-46b4-9559-eaa854590620")]
    public partial class CFRAMERepository : RepoGenBaseFolder
    {
        static CFRAMERepository instance = new CFRAMERepository();

        /// <summary>
        /// Gets the singleton class instance representing the CFRAMERepository element repository.
        /// </summary>
        [RepositoryFolder("e9b7fddc-ebe4-46b4-9559-eaa854590620")]
        public static CFRAMERepository Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Repository class constructor.
        /// </summary>
        public CFRAMERepository() 
            : base("CFRAMERepository", "/", null, 0, false, "e9b7fddc-ebe4-46b4-9559-eaa854590620", ".\\RepositoryImages\\CFRAMERepositorye9b7fddc.rximgres")
        {
        }

#region Variables

#endregion

        /// <summary>
        /// The Self item info.
        /// </summary>
        [RepositoryItemInfo("e9b7fddc-ebe4-46b4-9559-eaa854590620")]
        public virtual RepoItemInfo SelfInfo
        {
            get
            {
                return _selfInfo;
            }
        }
    }

    /// <summary>
    /// Inner folder classes.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")]
    public partial class CFRAMERepositoryFolders
    {
    }
#pragma warning restore 0436
}