﻿'//////////////////////////////////////////////////////////////////////////////
'
' This file was automatically generated by RANOREX.
' DO NOT MODIFY THIS FILE! It is regenerated by the designer.
' All your modifications will be lost!
' http://www.ranorex.com
'
'//////////////////////////////////////////////////////////////////////////////

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Drawing
Imports Ranorex
Imports Ranorex.Core
Imports Ranorex.Core.Repository
Imports Ranorex.Core.Testing

Namespace webcamnew
    ''' <summary>
    ''' The class representing the webcamnewRepository element repository.
    ''' </summary>
    <System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1"), _
    RepositoryFolder("91c73fe4-d861-49c6-890a-2bade23d438b")> _
    Partial Public Class webcamnewRepository
        Inherits RepoGenBaseFolder

        Shared _instance As webcamnewRepository = New webcamnewRepository()
        Dim _cframe2 As webcamnewRepositoryFolders.CFRAME2AppFolder

        ''' <summary>
        ''' Repository class constructor.
        ''' </summary>
        Public Sub New()
            MyBase.New("webcamnewRepository", "/", Nothing, 0, False,"91c73fe4-d861-49c6-890a-2bade23d438b", ".\RepositoryImages\webcamnewRepository91c73fe4.rximgres")
            _cframe2 = New webcamnewRepositoryFolders.CFRAME2AppFolder(Me)
        End Sub

        ''' <summary>
        ''' Gets the singleton class instance representing the webcamnewRepository element repository.
        ''' </summary>
        <RepositoryFolder("91c73fe4-d861-49c6-890a-2bade23d438b")> _
        Public Shared ReadOnly Property Instance() As webcamnewRepository
            Get
                Return _instance
            End Get
        End Property

#Region "Variables"
#End Region

        ''' <summary>
        ''' The Self item info.
        ''' </summary>
        <RepositoryItemInfo("91c73fe4-d861-49c6-890a-2bade23d438b")> _
        Public Overridable ReadOnly Property SelfInfo() As RepoItemInfo
            Get
                Return _selfInfo
            End Get
        End Property

        ''' <summary>
        ''' The CFRAME2 folder.
        ''' </summary>
        <RepositoryFolder("67e99838-1837-4bea-81a3-4801363e100e")> _
        Public Overridable ReadOnly Property CFRAME2 As webcamnewRepositoryFolders.CFRAME2AppFolder
            Get
                Return _cframe2
            End Get
        End Property
    End Class

    ''' <summary>
    ''' Inner folder classes.
    ''' </summary>
    <System.CodeDom.Compiler.GeneratedCode("Ranorex", "7.1")> _
    Partial Public Class webcamnewRepositoryFolders

        ''' <summary>
        ''' The CFRAME2AppFolder folder.
        ''' </summary>
        <RepositoryFolder("67e99838-1837-4bea-81a3-4801363e100e")> _
        Partial Public Class CFRAME2AppFolder
            Inherits RepoGenBaseFolder

            Dim _list As webcamnewRepositoryFolders.ListFolder
            Dim _clearconfigInfo As RepoItemInfo
            Dim _clientInfo As RepoItemInfo
            Dim _reloadInfo As RepoItemInfo

            ''' <summary>
            ''' Creates a new CFRAME2  folder.
            ''' </summary>
            Public Sub New(parentFolder As RepoGenBaseFolder)
                MyBase.New("CFRAME2", "/form[@title~'^C\.FRAME\ 2\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ']", parentFolder, 30000, Nothing, True, "67e99838-1837-4bea-81a3-4801363e100e", ".\RepositoryImages\webcamnewRepository91c73fe4.rximgres")
                _list = New webcamnewRepositoryFolders.ListFolder(Me)
                _clearconfigInfo = New RepoItemInfo(Me, "ClearConfig", "?/?/button[@accessiblename='Clear Config']", 30000, Nothing, "9ed9515a-5fdb-466b-8e24-598f3249b5c5")
                _clientInfo = New RepoItemInfo(Me, "Client", "container[2]/container[@accessiblerole='Client']", 30000, Nothing, "c1657e21-2cc7-46fe-89af-805342ad9a76")
                _reloadInfo = New RepoItemInfo(Me, "Reload", "?/?/button[@accessiblename='Reload']", 30000, Nothing, "2c014d55-c7bc-4392-be8a-6af103952ddf")
            End Sub

            ''' <summary>
            ''' The Self item.
            ''' </summary>
            <RepositoryItem("67e99838-1837-4bea-81a3-4801363e100e")> _
            Public Overridable ReadOnly Property Self As Ranorex.Form
                Get
                    Return _selfInfo.CreateAdapter(Of Ranorex.Form)(True)
                End Get
            End Property

            ''' <summary>
            ''' The Self item info.
            ''' </summary>
            <RepositoryItemInfo("67e99838-1837-4bea-81a3-4801363e100e")> _
            Public Overridable ReadOnly Property SelfInfo As RepoItemInfo
                Get
                    Return _selfInfo
                End Get
            End Property

            ''' <summary>
            ''' The ClearConfig item.
            ''' </summary>
            <RepositoryItem("9ed9515a-5fdb-466b-8e24-598f3249b5c5")> _
            Public Overridable ReadOnly Property ClearConfig As Ranorex.Button
                Get
                    Return _clearconfigInfo.CreateAdapter(Of Ranorex.Button)(True)
                End Get
            End Property

            ''' <summary>
            ''' The ClearConfig item info.
            ''' </summary>
            <RepositoryItemInfo("9ed9515a-5fdb-466b-8e24-598f3249b5c5")> _
            Public Overridable ReadOnly Property ClearConfigInfo As RepoItemInfo
                Get
                    Return _clearconfigInfo
                End Get
            End Property

            ''' <summary>
            ''' The Client item.
            ''' </summary>
            <RepositoryItem("c1657e21-2cc7-46fe-89af-805342ad9a76")> _
            Public Overridable ReadOnly Property Client As Ranorex.Container
                Get
                    Return _clientInfo.CreateAdapter(Of Ranorex.Container)(True)
                End Get
            End Property

            ''' <summary>
            ''' The Client item info.
            ''' </summary>
            <RepositoryItemInfo("c1657e21-2cc7-46fe-89af-805342ad9a76")> _
            Public Overridable ReadOnly Property ClientInfo As RepoItemInfo
                Get
                    Return _clientInfo
                End Get
            End Property

            ''' <summary>
            ''' The Reload item.
            ''' </summary>
            <RepositoryItem("2c014d55-c7bc-4392-be8a-6af103952ddf")> _
            Public Overridable ReadOnly Property Reload As Ranorex.Button
                Get
                    Return _reloadInfo.CreateAdapter(Of Ranorex.Button)(True)
                End Get
            End Property

            ''' <summary>
            ''' The Reload item info.
            ''' </summary>
            <RepositoryItemInfo("2c014d55-c7bc-4392-be8a-6af103952ddf")> _
            Public Overridable ReadOnly Property ReloadInfo As RepoItemInfo
                Get
                    Return _reloadInfo
                End Get
            End Property

            ''' <summary>
            ''' The List folder.
            ''' </summary>
            <RepositoryFolder("3f011876-121d-44a4-accb-951212f474ea")> _
            Public Overridable ReadOnly Property List As webcamnewRepositoryFolders.ListFolder
                Get
                    Return _list
                End Get
            End Property
        End Class

        ''' <summary>
        ''' The ListFolder folder.
        ''' </summary>
        <RepositoryFolder("3f011876-121d-44a4-accb-951212f474ea")> _
        Partial Public Class ListFolder
            Inherits RepoGenBaseFolder

            Dim _idscameraInfo As RepoItemInfo
            Dim _videowidgetInfo As RepoItemInfo
            Dim _webcamInfo As RepoItemInfo

            ''' <summary>
            ''' Creates a new List  folder.
            ''' </summary>
            Public Sub New(parentFolder As RepoGenBaseFolder)
                MyBase.New("List", "container[1]//list[@accessiblerole='List']", parentFolder, 30000, Nothing, False, "3f011876-121d-44a4-accb-951212f474ea", ".\RepositoryImages\webcamnewRepository91c73fe4.rximgres")
                _idscameraInfo = New RepoItemInfo(Me, "IDSCamera", "listitem[@accessiblename='IDSCamera']", 30000, Nothing, "0cd572c9-6c2e-4113-ac5e-7bc903af87fb")
                _videowidgetInfo = New RepoItemInfo(Me, "VideoWidget", "listitem[@accessiblename='VideoWidget']", 30000, Nothing, "47321c04-bba6-40d8-9065-f337dac698b1")
                _webcamInfo = New RepoItemInfo(Me, "Webcam", "listitem[@accessiblename='Webcam']", 30000, Nothing, "9e0e4a24-8ad4-4516-bd0f-10a847dbddbc")
            End Sub

            ''' <summary>
            ''' The Self item.
            ''' </summary>
            <RepositoryItem("3f011876-121d-44a4-accb-951212f474ea")> _
            Public Overridable ReadOnly Property Self As Ranorex.List
                Get
                    Return _selfInfo.CreateAdapter(Of Ranorex.List)(True)
                End Get
            End Property

            ''' <summary>
            ''' The Self item info.
            ''' </summary>
            <RepositoryItemInfo("3f011876-121d-44a4-accb-951212f474ea")> _
            Public Overridable ReadOnly Property SelfInfo As RepoItemInfo
                Get
                    Return _selfInfo
                End Get
            End Property

            ''' <summary>
            ''' The IDSCamera item.
            ''' </summary>
            <RepositoryItem("0cd572c9-6c2e-4113-ac5e-7bc903af87fb")> _
            Public Overridable ReadOnly Property IDSCamera As Ranorex.ListItem
                Get
                    Return _idscameraInfo.CreateAdapter(Of Ranorex.ListItem)(True)
                End Get
            End Property

            ''' <summary>
            ''' The IDSCamera item info.
            ''' </summary>
            <RepositoryItemInfo("0cd572c9-6c2e-4113-ac5e-7bc903af87fb")> _
            Public Overridable ReadOnly Property IDSCameraInfo As RepoItemInfo
                Get
                    Return _idscameraInfo
                End Get
            End Property

            ''' <summary>
            ''' The VideoWidget item.
            ''' </summary>
            <RepositoryItem("47321c04-bba6-40d8-9065-f337dac698b1")> _
            Public Overridable ReadOnly Property VideoWidget As Ranorex.ListItem
                Get
                    Return _videowidgetInfo.CreateAdapter(Of Ranorex.ListItem)(True)
                End Get
            End Property

            ''' <summary>
            ''' The VideoWidget item info.
            ''' </summary>
            <RepositoryItemInfo("47321c04-bba6-40d8-9065-f337dac698b1")> _
            Public Overridable ReadOnly Property VideoWidgetInfo As RepoItemInfo
                Get
                    Return _videowidgetInfo
                End Get
            End Property

            ''' <summary>
            ''' The Webcam item.
            ''' </summary>
            <RepositoryItem("9e0e4a24-8ad4-4516-bd0f-10a847dbddbc")> _
            Public Overridable ReadOnly Property Webcam As Ranorex.ListItem
                Get
                    Return _webcamInfo.CreateAdapter(Of Ranorex.ListItem)(True)
                End Get
            End Property

            ''' <summary>
            ''' The Webcam item info.
            ''' </summary>
            <RepositoryItemInfo("9e0e4a24-8ad4-4516-bd0f-10a847dbddbc")> _
            Public Overridable ReadOnly Property WebcamInfo As RepoItemInfo
                Get
                    Return _webcamInfo
                End Get
            End Property
        End Class
    End Class
End Namespace
